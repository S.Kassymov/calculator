package com.example.calculator

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var isNewOp=true
    var oldNumb=""
    var op="+"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun numberEvent(view: View) {
        if(isNewOp)
            editText.setText("")
        isNewOp=false
        var bclick:String= editText.text.toString()
        var bselect:Button=view as Button
        when(bselect.id){
            one.id->{bclick+="1"}
            two.id->{bclick+="2"}
            three.id->{bclick+="3"}
            four.id->{bclick+="4"}
            five.id->{bclick+="5"}
            six.id->{bclick+="6"}
            seven.id->{bclick+="7"}
            eight.id->{bclick+="8"}
            nine.id->{bclick+="9"}
            zero.id->{bclick+="0"}
            dot.id->{bclick+="."}
            plus.id->{bclick+="+"}
            minus.id->{bclick+="-"}
        }
        editText.setText(bclick)
    }

    fun operatorEvent(view: View) {
        isNewOp=true
        oldNumb=editText.text.toString()
        var bselect:Button=view as Button
        when(bselect.id){
            multiply.id->{op="*"}
            plus.id->{op="+"}
            minus.id->{op="-"}
            divide.id->{op="/"}
        }
    }

    fun equalEvent(view: View) {
        var newnum:String=editText.text.toString()
        var result=0.0
        when(op){
            "+"->{result=oldNumb.toDouble()+newnum.toDouble()}
            "-"->{result=oldNumb.toDouble()-newnum.toDouble()}
            "*"->{result=oldNumb.toDouble()*newnum.toDouble()}
            "/"->{result=oldNumb.toDouble()/newnum.toDouble()}
        }
        editText.setText(result.toString())
    }

    fun ClearEvent(view: View) {
        editText.setText("0")
        isNewOp=true
    }

    fun percentEvent(view: View) {
        var no:Double=editText.text.toString().toDouble()/100
        editText.setText(no.toString())
        isNewOp=true
    }

    fun deleteEvent(view: View) {
        var newN=""
        newN = if (oldNumb.length > 1) oldNumb.substring(0, oldNumb.length - 1) else ""
        editText.setText(newN)
    }

}